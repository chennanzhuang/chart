package com.ilucky.chart.jfreechart.export.two;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.util.TableOrder;

import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author IluckySi
 * @date 20140724
 */
public class ExportToPdf {

	private static final String PDF = ".pdf";
	
	private String destination;
	
	private String name;
	
	private int imageWidth;
	
	private int imageHeight;
	
	private String imageStyle;
	
	private Document document;
	
	private int countsPerPage;
	
	private StandardChartTheme standardChartTheme;
	
	public ExportToPdf() {
		this.imageWidth = 900;
		this.imageHeight = 540;
		this.imageStyle = "png";
		this.countsPerPage = 2;
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}

	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}

	public void setImageStyle(String imageStyle) {
		this.imageStyle = imageStyle;
	}

	public void setCountsPerPage(int countsPerPage) {
		this.countsPerPage = countsPerPage;
	}
	
	public void setStandardChartTheme(StandardChartTheme standardChartTheme) {
		this.standardChartTheme = standardChartTheme;
	}
	
	public void createDocument() {
		try {
			//创建目标文件.
			File file = new File(destination + "/" + name + PDF);
			
			//创建Document,并设置页边距.
			Rectangle pagesize = new Rectangle(imageWidth + 100, imageHeight * countsPerPage + 150);  
			document = new Document(pagesize, 50, 50, 50, 50);
			
			//将目标文件和Document关联书写器, 注意: 书写器和导出的文件格式息息相关.
			PdfWriter.getInstance(document, new FileOutputStream(file));
			
			//打开document.
			document.open();
			
			//应用主题样式.
			ChartFactory.setChartTheme(standardChartTheme);
		} catch (Exception e) {
			System.out.println("创建Document发生错误!");
		}
	}
	
	//二维2D饼状图参数: 图表标题-数据集-图表方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addPieMulit2D(String title, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createMultiplePieChart(title, categoryDataSet,  TableOrder.BY_ROW, true, false, false);
		
		//将报表添加到Document中.
		this.add(pieChart);
	}
	
	//二维3D饼状图参数: 图表标题-数据集-图表方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addPieMulti3D(String title, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createMultiplePieChart3D(title, categoryDataSet,  TableOrder.BY_ROW, true, false, false);
		
		//将报表添加到Document中.
		this.add(pieChart);
	}
	
	//二维2D柱状图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addBar2D(String title, String x, String y, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createBarChart(title, x, y, categoryDataSet, PlotOrientation.VERTICAL, true, false, false);
		
		//将报表添加到Document中.
		this.add(pieChart);
	}
	
	//二维3D柱状图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addBar3D(String title, String x, String y, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createBarChart3D(title, x, y, categoryDataSet, PlotOrientation.VERTICAL, true, false, false);
		
		//将报表添加到Document中.
		this.add(pieChart);
	}
	
	//二维2D折线图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addLine2D(String title, String x, String y, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createLineChart(title, x, y, categoryDataSet, PlotOrientation.VERTICAL, true, false, false);
		
		//将报表添加到Document中.
		this.add(pieChart);
	}
		
	//二维3D柱状图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addLine3D(String title, String x, String y, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createLineChart3D(title, x, y, categoryDataSet, PlotOrientation.VERTICAL, true, false, false);
		
		//将报表添加到Document中.
		this.add(pieChart);
	}
	
	private  void add(JFreeChart chart) {
		ByteArrayOutputStream baos = null;
		try {
			//将报表写入到图片缓冲区中,并设置大小.
			BufferedImage bi = chart.createBufferedImage(imageWidth, imageHeight);
			
			//将缓冲区中的图片写入到字节输出流.
			baos = new ByteArrayOutputStream();
			ImageIO.write(bi, imageStyle, baos);
			
			//将字节输出流转换为字节数组.
			byte[] data = baos.toByteArray();
			
			//将字节数组转换为Image,并写入到Document.
			Image image = Image.getInstance(data);
			document.add(image);
		} catch (Exception e) {
			System.out.println("向word添加报表发生错误!");
		} finally {
			try {
				if(baos != null) {
					baos.close();
					baos = null;
				}
			} catch (IOException e) {
				System.out.println("关闭流发生错误!");
			}
		}
	}
	
	public void closeDocument() {
		try {
			if(document != null) {
				document.close();
				document = null;
			} 
		} catch (Exception e) {
			System.out.println("关闭Document发生错误!");
		}
	}
}
