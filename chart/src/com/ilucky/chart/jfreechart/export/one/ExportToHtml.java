package com.ilucky.chart.jfreechart.export.one;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 * @author IluckySi
 * @date 20140724
 */
public class ExportToHtml {

	private static final String HTML = ".html";
	
	private String destination;
	
	private String name;
	
	private int imageWidth;
	
	private int imageHeight;
	
	private String imageStyle;
	
	private StandardChartTheme standardChartTheme;
	
	private String imageStart;
	
	private String imageEnd;
	
	private FileOutputStream fos = null;
	
	private BufferedOutputStream bos = null;
	
	private PrintWriter pw = null; 
	
	public ExportToHtml() {
		this.imageWidth = 900;
		this.imageHeight = 540;
		this.imageStyle = "png";
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}

	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}

	public void setImageStyle(String imageStyle) {
		this.imageStyle = imageStyle;
	}

	public void setStandardChartTheme(StandardChartTheme standardChartTheme) {
		this.standardChartTheme = standardChartTheme;
	}
	
	public void createStream() {
		try {
			//创建目标文件.
			File file = new File(destination + "/" + name + HTML);
			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			pw = new PrintWriter(bos);
			
			//设置html的图片属性.
			imageStart = "<div align=\"center\"><img src=\"";
			imageEnd = "\" width=\"" + imageWidth  + "\" height=\"" + imageHeight + "\" border=\"0\"></div></br>";
			
			//应用主题样式.
			ChartFactory.setChartTheme(standardChartTheme);
		} catch (Exception e) {
			System.out.println("创建文件流发生错误!");
		}
	}
	
	//一维2D饼状图参数: 图表标题-数据集-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addPie2D(String title, DefaultPieDataset pieDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createPieChart(title, pieDataSet, true, false, false);
		
		//将报表添加到html中.
		this.add(pieChart, "1-2D-pie");
	}
	
	//一维3D饼状图参数: 图表标题-数据集-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addPie3D(String title, DefaultPieDataset pieDataSet) {
		//创建报表.
		JFreeChart pieChart = ChartFactory.createPieChart3D(title, pieDataSet, true, false, false);
		
		//将报表添加到html中.
		this.add(pieChart, "1-3D-pie");
	}

	//一维2D柱状图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addBar2D(String title, String x, String y, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart barChart = ChartFactory.createBarChart(title, x, y, categoryDataSet, PlotOrientation.VERTICAL, true, false, false);
		
		//将报表添加到html中.
		this.add(barChart, "1-2D-bar");
	}
	
	//一维3D柱状图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addBar3D(String title, String x, String y, DefaultCategoryDataset categoryDataSet) {
		//创建报表.
		JFreeChart barChart = ChartFactory.createBarChart3D(title, x, y, categoryDataSet, PlotOrientation.VERTICAL, true, false, false);

		//将报表添加到html中.
		this.add(barChart, "1-3D-bar");
	}

	//一维2D折线图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addLine2D(String title, String x, String y, DefaultCategoryDataset lineDataSet) {
		//创建报表.
		JFreeChart lineChart = ChartFactory.createLineChart(title, x, y, lineDataSet, PlotOrientation.VERTICAL, true, false, false);
		
		//将报表添加到html中.
		this.add(lineChart, "1-2D-line");
	}
	
	//一维3D折线图参数: 图表标题-x轴标题-y轴标题-数据集-显示方向-是否显示图例-是否采用标准生成器-是否生成链接.
	public void addLine3D(String title, String x, String y, DefaultCategoryDataset lineDataSet) {
		//创建报表.
		JFreeChart lineChart = ChartFactory.createLineChart3D(title, x, y, lineDataSet, PlotOrientation.VERTICAL, true, false, false);
		
		//将报表添加到html中.
		this.add(lineChart, "1-3D-line");
	}
	
	private  void add(JFreeChart chart, String name) {
		try {
			//根据图片格式保存图片.
			final ChartRenderingInfo cr = new ChartRenderingInfo(new StandardEntityCollection());
			if("png".equals(imageStyle)) {
				File file = new File(destination + "/" + name);
				ChartUtilities.saveChartAsPNG(file, chart,  imageWidth, imageHeight, cr);
			} else {
				File file = new File(destination + "/" + name);
				ChartUtilities.saveChartAsJPEG(file, chart,  imageWidth, imageHeight, cr);
			}
			
			//将报表添加到html中.
			pw.println(imageStart);
			pw.println(name);
			pw.println(imageEnd);
		} catch (Exception e) {
			System.out.println("向html添加报表发生错误!");
		} 
	}
	
	public void closeStream() {
		try {
			if(pw != null) {
				pw.close();
				pw = null;
			}
			if(bos != null) {
				bos.close();
				bos = null;
			}
			if(fos != null) {
				fos.close();
				fos = null;
			}
		} catch (Exception e) {
			System.out.println("关闭文件流发生错误!");
		}
	}
}
