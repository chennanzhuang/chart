package com.ilucky.chart.jfreechart.data.two;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.data.category.DefaultCategoryDataset;

/**
 * @author IluckySi
 * @date 20140724
 */
public class Bar {
	
	public static DefaultCategoryDataset getDataset(List<Map<String, String>> list, List<String> names) {
		DefaultCategoryDataset defaultPieDataset = new DefaultCategoryDataset();
		for(int i = 0; list != null && names != null && i < list.size(); i++) {
			if(list.size() != names.size()) {
				try {
					throw new Exception("数据源有问题!");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			Map<String, String> map = list.get(i);
			String name = names.get(i);
			for(Entry<String, String> entry : map.entrySet()) {
				defaultPieDataset.addValue(Double.parseDouble(entry.getValue()), entry.getKey(), name);
			}
			for(Entry<String, String> entry : map.entrySet()) {
				defaultPieDataset.addValue(Double.parseDouble(entry.getValue()), entry.getKey(), name);
			}
			for(Entry<String, String> entry : map.entrySet()) {
				defaultPieDataset.addValue(Double.parseDouble(entry.getValue()), entry.getKey(), name);
			}
		}
		return defaultPieDataset;
	}
}
