package com.ilucky.chart.jfreechart.data.one;

import java.util.Map;
import java.util.Map.Entry;

import org.jfree.data.general.DefaultPieDataset;

/**
 * @author IluckySi
 * @date 20140723
 */
public class Pie {

	public static DefaultPieDataset getDataset(Map<String, String> dataSource) {
		DefaultPieDataset defaultPieDataset = new DefaultPieDataset();
		for(Entry<String, String> entry : dataSource.entrySet()) {
			defaultPieDataset.setValue(entry.getKey(), Double.parseDouble(entry.getValue()));
		}
		return defaultPieDataset;
	}
}
