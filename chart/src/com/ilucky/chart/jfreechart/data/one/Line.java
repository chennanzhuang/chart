package com.ilucky.chart.jfreechart.data.one;

import java.util.Map;
import java.util.Map.Entry;

import org.jfree.data.category.DefaultCategoryDataset;

/**
 * @author IluckySi
 * @date 20140723
 */
public class Line {
	
	public static DefaultCategoryDataset getDataset(Map<String, String> dataSource, String name) {
		DefaultCategoryDataset defaultineDataset = new DefaultCategoryDataset();
		for(Entry<String, String> entry : dataSource.entrySet()) {
			defaultineDataset.setValue(Integer.parseInt(entry.getValue()), name, entry.getKey());
		}
		return defaultineDataset;
	}
}
