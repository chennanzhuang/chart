package com.ilucky.chart.jfreechart.data.one;

import java.util.Map;
import java.util.Map.Entry;

import org.jfree.data.category.DefaultCategoryDataset;

/**
 * @author IluckySi
 * @date 20140723
 */
public class Bar {

	public static DefaultCategoryDataset getDataset(Map<String, String> dataSource) {
		DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();
		for(Entry<String, String> entry : dataSource.entrySet()) {
			defaultCategoryDataset.setValue(Double.parseDouble(entry.getValue()), entry.getKey(), entry.getKey());
		}
		return defaultCategoryDataset;
	}
}
