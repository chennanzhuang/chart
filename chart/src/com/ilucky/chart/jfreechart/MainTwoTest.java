package com.ilucky.chart.jfreechart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jfree.chart.StandardChartTheme;
import org.jfree.data.category.DefaultCategoryDataset;

import com.ilucky.chart.jfreechart.data.two.Bar;
import com.ilucky.chart.jfreechart.data.two.Line;
import com.ilucky.chart.jfreechart.data.two.Pie;
import com.ilucky.chart.jfreechart.export.two.ExportToHtml;
import com.ilucky.chart.jfreechart.export.two.ExportToPdf;
import com.ilucky.chart.jfreechart.export.two.ExportToWord;
import com.ilucky.chart.jfreechart.theme.ChartTheme;

/**
 * @author IluckySi
 * @date 20140723
 */
public class MainTwoTest {

	public static void main(String[] args) {
		
		//模拟数据源: 月份-事件类型-事件数量.
		Map<String, String> dataSource1 = new HashMap<String, String>();
		dataSource1.put("cpu资源不足风险事件", "10");
		dataSource1.put("内存资源不足风险事件", "20");
		dataSource1.put("硬盘资源不足风险事件", "30");
		Map<String, String> dataSource2 = new HashMap<String, String>();
		dataSource2.put("cpu资源不足风险事件", "30");
		dataSource2.put("内存资源不足风险事件", "20");
		dataSource2.put("硬盘资源不足风险事件", "10");
		Map<String, String> dataSource3 = new HashMap<String, String>();
		dataSource3.put("cpu资源不足风险事件", "20");
		dataSource3.put("内存资源不足风险事件", "20");
		dataSource3.put("硬盘资源不足风险事件", "20");
		
		//组装数据源.
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		list.add(dataSource1);
		list.add(dataSource2);
		list.add(dataSource3);
		List<String> names = new ArrayList<String>();
		names.add("八月份");
		names.add("九月份");
		names.add("十月份");
		
		//创建饼状图数据源,创建多重饼图必须把PieDataSet改成CategoryDataSet.
		DefaultCategoryDataset defaultPieDataset =Pie.getDataset(list, names);
				
		//创建柱状图数据源.
		DefaultCategoryDataset defaultCategoryDataset = Bar.getDataset(list, names);
		
		//创建折线图数据源.
		DefaultCategoryDataset defaultLineDataset = Line.getDataset(list, names);
		
		//导出为word.
		System.out.println("开始导出word!");
		ExportToWord exportToWord = new ExportToWord();
		exportToWord.setDestination("D:/chart");
		exportToWord.setName("one_word2");
		exportToWord.setImageWidth(900);
		exportToWord.setImageHeight(540);
		exportToWord.setCountsPerPage(2);
		exportToWord.setImageStyle("png");
		ChartTheme wordTheme = new ChartTheme();
		wordTheme.setExtraLargetFont(16);
		wordTheme.setRegularFont(14);
		wordTheme.setLargetFont(14);
		StandardChartTheme wordStandardChartTheme = wordTheme.getChartTheme();
		exportToWord.setStandardChartTheme(wordStandardChartTheme);
		exportToWord.createDocument();
		exportToWord.addPieMulit2D("风险事件统计饼状图", defaultPieDataset);
		exportToWord.addPieMulti3D("风险事件统计饼状图", defaultPieDataset);
		exportToWord.addBar2D("风险事件统计柱状图", "事件类型", "事件数量", defaultCategoryDataset);
		exportToWord.addBar3D("风险事件统计柱状图", "事件类型", "事件数量", defaultCategoryDataset);
		exportToWord.addLine2D("风险事件统计柱状图", "事件类型", "事件数量", defaultLineDataset);
		exportToWord.addLine3D("风险事件统计柱状图", "事件类型", "事件数量", defaultLineDataset);
		exportToWord.closeDocument();
		System.out.println("导出word结束!");
		
		//导出为pdf.
		System.out.println("开始导出pdf!");
		ExportToPdf exportToPdf = new ExportToPdf();
		exportToPdf.setDestination("D:/chart");
		exportToPdf.setName("one_pdf2");
		exportToPdf.setImageWidth(900);
		exportToPdf.setImageHeight(540);
		exportToPdf.setCountsPerPage(2);
		ChartTheme pdfTheme = new ChartTheme();
		pdfTheme.setExtraLargetFont(16);
		pdfTheme.setRegularFont(14);
		pdfTheme.setLargetFont(14);
		StandardChartTheme pdfStandardChartTheme = pdfTheme.getChartTheme();
		exportToPdf.setStandardChartTheme(pdfStandardChartTheme);
		exportToPdf.createDocument();
		exportToPdf.addPieMulit2D("风险事件统计饼状图", defaultPieDataset);
		exportToPdf.addPieMulti3D("风险事件统计饼状图", defaultPieDataset);
		exportToPdf.addBar2D("风险事件统计柱状图", "事件类型", "事件数量", defaultCategoryDataset);
		exportToPdf.addBar3D("风险事件统计柱状图", "事件类型", "事件数量", defaultCategoryDataset);
		exportToPdf.addLine2D("风险事件统计柱状图", "事件类型", "事件数量", defaultLineDataset);
		exportToPdf.addLine3D("风险事件统计柱状图", "事件类型", "事件数量", defaultLineDataset);
		exportToPdf.closeDocument();
		System.out.println("导出pdf结束!");
		
		//导出为html.
		System.out.println("开始导出html!");
		ExportToHtml exportToHtml = new ExportToHtml();
		exportToHtml.setDestination("D:/chart");
		exportToHtml.setName("one_html2");
		exportToHtml.setImageWidth(900);
		exportToHtml.setImageHeight(540);
		exportToHtml.setImageStyle("png");
		ChartTheme htmlTheme = new ChartTheme();
		htmlTheme.setExtraLargetFont(16);
		htmlTheme.setRegularFont(14);
		htmlTheme.setLargetFont(14);
		StandardChartTheme htmlStandardChartTheme = htmlTheme.getChartTheme();
		exportToHtml.setStandardChartTheme(htmlStandardChartTheme);
		exportToHtml.createStream();
		exportToHtml.addPieMulit2D("风险事件统计饼状图", defaultPieDataset);
		exportToHtml.addPieMulti3D("风险事件统计饼状图", defaultPieDataset);
		exportToHtml.addBar2D("风险事件统计柱状图", "事件类型", "事件数量", defaultCategoryDataset);
		exportToHtml.addBar3D("风险事件统计柱状图", "事件类型", "事件数量", defaultCategoryDataset);
		exportToHtml.addLine2D("风险事件统计柱状图", "事件类型", "事件数量", defaultLineDataset);
		exportToHtml.addLine3D("风险事件统计柱状图", "事件类型", "事件数量", defaultLineDataset);
		exportToHtml.closeStream();
		System.out.println("导出word结束!");
	}
}
