package com.ilucky.chart.jfreechart.theme;

import java.awt.Font;
import java.awt.RenderingHints;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;

/**
 * @author IluckySi
 * @date 20140724
 */
public class ChartTheme  {

	private int extraLargetFont;
	
	private int regularFont;
	
	private int largetFont;
	
	public ChartTheme () {
		this.extraLargetFont = 12;
		this.regularFont = 10;
		this.largetFont = 10;
	}
	
	public void setExtraLargetFont(int extraLargetFont) {
		this.extraLargetFont = extraLargetFont;
	}

	public void setRegularFont(int regularFont) {
		this.regularFont = regularFont;
	}

	public void setLargetFont(int largetFont) {
		this.largetFont = largetFont;
	}

	public StandardChartTheme getChartTheme() {
	
		StandardChartTheme standardChartTheme = new StandardChartTheme("CN") {
			
			private static final long serialVersionUID = 1L;

			//消除文字锯齿.
			public void apply(JFreeChart chart) {
				chart.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
				super.apply(chart);
			}
		};
		
		//创建主题样式,标题字体,图例字体,轴向字体.
		standardChartTheme.setExtraLargeFont(new Font("宋体", Font.BOLD, extraLargetFont));
		standardChartTheme.setRegularFont(new Font("宋体", Font.PLAIN, regularFont));
		standardChartTheme.setLargeFont(new Font("宋体", Font.PLAIN, largetFont));
		return standardChartTheme;
	}
}
