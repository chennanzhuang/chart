package com.ilucky.chart.jfreechart;

import java.util.HashMap;
import java.util.Map;

import org.jfree.chart.StandardChartTheme;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import com.ilucky.chart.jfreechart.data.one.Bar;
import com.ilucky.chart.jfreechart.data.one.Line;
import com.ilucky.chart.jfreechart.data.one.Pie;
import com.ilucky.chart.jfreechart.export.one.ExportToHtml;
import com.ilucky.chart.jfreechart.export.one.ExportToPdf;
import com.ilucky.chart.jfreechart.export.one.ExportToWord;
import com.ilucky.chart.jfreechart.theme.ChartTheme;

/**
 * @author IluckySi
 * @date 20140723
 */
public class MainOneTest {

	public static void main(String[] args) {
		
		//模拟数据源: 事件类型.
		Map<String, String> dataSource = new HashMap<String, String>();
		dataSource.put("cpu资源不足风险事件", "10");
		dataSource.put("内存资源不足风险事件", "20");
		dataSource.put("硬盘资源不足风险事件", "30");
		
		//获取饼状图,柱状图和折线图数据源.
		DefaultPieDataset defaultPieDataset = Pie.getDataset(dataSource);
		DefaultCategoryDataset defaultCategoryDataset = Bar.getDataset(dataSource);
		DefaultCategoryDataset defaultLineDataset = Line.getDataset(dataSource, "10月份");
		
		//导出为word.
		System.out.println("开始导出word!");
		ExportToWord exportToWord = new ExportToWord();
		exportToWord.setDestination("D:/chart");
		exportToWord.setName("one_word");
		exportToWord.setImageWidth(900);
		exportToWord.setImageHeight(540);
		exportToWord.setCountsPerPage(2);
		exportToWord.setImageStyle("png");
		ChartTheme wordTheme = new ChartTheme();
		wordTheme.setExtraLargetFont(16);
		wordTheme.setRegularFont(14);
		wordTheme.setLargetFont(14);
		StandardChartTheme wordStandardChartTheme = wordTheme.getChartTheme();
		exportToWord.setStandardChartTheme(wordStandardChartTheme);
		exportToWord.createDocument();
		exportToWord.addPie2D("风险事件统计饼状图2D", defaultPieDataset);
		exportToWord.addPie3D("风险事件统计饼状图3D", defaultPieDataset);
		exportToWord.addBar2D("风险事件统计柱状图2D", "事件类型", "事件数量", defaultCategoryDataset);
		exportToWord.addBar3D("风险事件统计柱状图3D", "事件类型", "事件数量", defaultCategoryDataset);
		exportToWord.addLine2D("风险事件统计折线图2D", "事件类型" , "事件数量", defaultLineDataset);
		exportToWord.addLine3D("风险事件统计折线图3D", "事件类型" , "事件数量", defaultLineDataset);
		exportToWord.closeDocument();
		System.out.println("导出word结束!");
		
		//导出为pdf.
		System.out.println("开始导出pdf!");
		ExportToPdf exportToPdf = new ExportToPdf();
		exportToPdf.setDestination("D:/chart");
		exportToPdf.setName("one_pdf");
		exportToPdf.setImageWidth(900);
		exportToPdf.setImageHeight(540);
		exportToPdf.setImageStyle("png");
		exportToPdf.setCountsPerPage(2);
		ChartTheme pdfTheme = new ChartTheme();
		pdfTheme.setExtraLargetFont(16);
		pdfTheme.setRegularFont(14);
		pdfTheme.setLargetFont(14);
		StandardChartTheme pdfStandardChartTheme = pdfTheme.getChartTheme();
		exportToPdf.setStandardChartTheme(pdfStandardChartTheme);
		exportToPdf.createDocument();
		exportToPdf.addPie2D("风险事件统计饼状图2D", defaultPieDataset);
		exportToPdf.addPie3D("风险事件统计饼状图3D", defaultPieDataset);
		exportToPdf.addBar2D("风险事件统计柱状图2D", "事件类型", "事件数量", defaultCategoryDataset);
		exportToPdf.addBar3D("风险事件统计柱状图3D", "事件类型", "事件数量", defaultCategoryDataset);
		exportToPdf.addLine2D("风险事件统计折线图2D", "事件类型" , "事件数量", defaultLineDataset);
		exportToPdf.addLine3D("风险事件统计折线图3D", "事件类型" , "事件数量", defaultLineDataset);
		exportToPdf.closeDocument();
		System.out.println("导出pdf结束!");
		
		//导出为html.
		System.out.println("开始导出html!");
		ExportToHtml exportToHtml = new ExportToHtml();
		exportToHtml.setDestination("D:/chart");
		exportToHtml.setName("one_html");
		exportToHtml.setImageWidth(900);
		exportToHtml.setImageHeight(540);
		exportToHtml.setImageStyle("png");
		ChartTheme htmlTheme = new ChartTheme();
		htmlTheme.setExtraLargetFont(16);
		htmlTheme.setRegularFont(14);
		htmlTheme.setLargetFont(14);
		StandardChartTheme htmlStandardChartTheme = htmlTheme.getChartTheme();
		exportToPdf.setStandardChartTheme(pdfStandardChartTheme);
		exportToHtml.setStandardChartTheme(htmlStandardChartTheme);
		exportToHtml.createStream();
		exportToHtml.addPie2D("风险事件统计饼状图2D", defaultPieDataset);
		exportToHtml.addPie3D("风险事件统计饼状图3D", defaultPieDataset);
		exportToHtml.addBar2D("风险事件统计柱状图2D", "事件类型", "事件数量", defaultCategoryDataset);
		exportToHtml.addBar3D("风险事件统计柱状图3D", "事件类型", "事件数量", defaultCategoryDataset);
		exportToHtml.addLine2D("风险事件统计折线图2D", "事件类型" , "事件数量", defaultLineDataset);
		exportToHtml.addLine3D("风险事件统计折线图3D", "事件类型" , "事件数量", defaultLineDataset);
		exportToHtml.closeStream();
		System.out.println("导出html结束!");
	}
}
